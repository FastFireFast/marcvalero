(function() {
  $('.menu1').on('click', function() {
      $('.menu1').css({"background-color": "#FFA800", "color": "black"})
      $('.menu3').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
      $('.menu4').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
      $('.menu2').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
      $("#pagines").css({"display": "none"});
      $(".contacte").css({"display": "none"});
      $(".cv").css({"display": "none"});
      $(".descarregarcv").css({"display": "none"});
      $(".portfoli").css({"display": "none"});
      $(".descarregarportfoli").css({"display": "none"});
      $(".introduccio").css({"display": "block"});
  });

  $('.menu4').on('click', function() {
    $('.menu4').css({"background-color": "#FFA800", "color": "black"})
    $('.menu3').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
    $('.menu1').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
    $('.menu2').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
    $("#pagines").css({"display": "none"});
    $(".introduccio").css({"display": "none"});
    $(".cv").css({"display": "none"});
    $(".descarregarcv").css({"display": "none"});
    $(".portfoli").css({"display": "none"});
    $(".descarregarportfoli").css({"display": "none"});
    $(".contacte").css({"display": "block"});
});

$('.menu2').on('click', function() {
  $('.menu2').css({"background-color": "#FFA800", "color": "black"})
  $('.menu3').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
  $('.menu4').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
  $('.menu1').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
  $("#pagines").css({"display": "none"});
  $(".contacte").css({"display": "none"});
  $(".introduccio").css({"display": "none"});
  $(".portfoli").css({"display": "none"});
  $(".descarregarportfoli").css({"display": "none"});
  $(".cv").css({"display": "block"});
  $(".descarregarcv").css({"display": "block"});

});

$('.menu3').on('click', function() {
  $('.menu3').css({"background-color": "#FFA800", "color": "black"})
  $('.menu2').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
  $('.menu4').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
  $('.menu1').css({"background-color": "rgba(160, 160, 160, 0.3)", "color": "#FFA800"})
  $("#pagines").css({"display": "none"});
  $(".contacte").css({"display": "none"});
  $(".introduccio").css({"display": "none"});
  $(".cv").css({"display": "none"});
  $(".descarregarcv").css({"display": "none"});
  $(".portfoli").css({"display": "block"});
  $(".descarregarportfoli").css({"display": "block"});

});

})();


function enviar(){

  var nom = $("#name").val();
  var cognom = $("#cognom").val();
  var empresa = $("#empresa").val();
  var email = $("#email").val();
  var asunto = "[WEB] Algu vol contactar amb tu!";
  var telefon = $("#telefon").val();
  var message = $("#message").val();
  
  Email.send({
    SecureToken : "37e3b27b-46d5-4b63-9945-b2e2934fe404",
    To : 'valeromarc@hotmail.com',
    From : email,
    Subject : asunto,
    Body : "Nom: " + nom + "<br>Cognom: " + cognom + "<br> Empresa: " + empresa + "<br> Telefon: " +
    telefon + "<br><br> <hr><br><h1>Cos del misatge</h1><br>" + message + "<br><br><br> Misatge rebut desde la web marcvalero.cat"
  }).then(
  message => alert(message)
  );
}


function myFunction(x) {
  if (x.matches) { // If media query matches
    $(".descarregarportfoli").css({"top": "7.5vh", "left": "2vw"});
    $(".descarregarcv").css({"top": "7.5vh", "left": "2vw"});
  } else{
    document.getElementById("download").className = "btn btn-warning";
    document.getElementById("download1").className = "btn btn-warning";
    $(".descarregarportfoli").css({"top": "5px", "left": "26vw"});
    $(".descarregarcv").css({"top": "5px", "left": "26vw"});
  }
}

var x = window.matchMedia("(max-width: 1000px)") //Si la finestra és menor a, o igual a, 1000 píxels d'amplada
myFunction(x) // Call listener function at run time
x.addListener(myFunction) // Attach listener function on state changes